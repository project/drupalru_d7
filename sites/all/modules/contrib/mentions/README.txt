Mentions
https://www.drupal.org/project/mentions
====================================

INTRODUCTION
------------

The Mentions module offers Twitter like functionality, recording all references
to a user's username - using the [@username] or [@#uid] filter format - from
various locations, providing a centralized page to track all mentions.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:

https://www.drupal.org/docs/7/extend/installing-modules

for further information.

FEATURES
--------

* Tracks Mentions on any Entity.
* An Input filter to convert [@username] or [@#uid] to the user's profile.
* Customizable input ([@username], [@#uid]) and output (@username) patterns,
  including support for the Token module.
* Integrations with:
  * Machine name - Use a Machine name field as the mention source.
  * Rules - React to created, updated and deleted mentions.
  * Views - Display a list of all mentions, mentions by user, and more.

REQUIREMENTS
------------

filter (core)

RECOMMENDED MODULES
-------------------

* Libraries API - https://drupal.org/project/libraries
* Token         - https://drupal.org/project/token
* Views         - https://drupal.org/project/views

CONFIGURATION
-------------

Once installed, the Mentions filter needs to be enabled on your desired Text
formats, this can be done via the Text formats page.
* http://[www.yoursite.com/path/to/drupal]/admin/config/content/formats

Customisation settings for input and output patterns are available via the
Mentions configuration form:
* http://[www.yoursite.com/path/to/drupal]/admin/config/content/mentions

MAINTAINERS
-----------

Current maintainers:
* Andrei Ivnitskii - https://www.drupal.org/u/ivnish
