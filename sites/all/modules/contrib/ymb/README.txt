Yandex Money Blocks (YMB)
https://www.drupal.org/project/ymb
==================================

INTRODUCTION
------------

Module allows creation of blocks with form for raising money to your Yandex
Money Wallet.

REQUIREMENTS
------------

block (core)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------

Go to admin/config/content/ymb and set count of blocks.

Go to admin/structure/block and manage YMB-blocks.

MAINTAINERS
-----------

Current maintainers:
* Andrei Ivnitskii - https://www.drupal.org/u/itcrowd72
